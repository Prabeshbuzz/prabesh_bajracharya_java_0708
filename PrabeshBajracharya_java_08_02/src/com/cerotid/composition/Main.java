package com.cerotid.composition;

public class Main {

	public static void main(String[] args) {
		Dimensions dimensions = new Dimensions(20,20,5);
		Case theCase = new Case("2207","Dell","345",dimensions);
		
		Resolution resolution = new Resolution(1024, 680);
		Monitor theMonitor = new Monitor("22 inch", "Acer", 27, resolution);
		
		Motherboard motherboard = new Motherboard("core i3", "intel", 4, 6, " V2.0");
		
		Laptop laptop =new Laptop(theCase, theMonitor, motherboard);
		
		laptop.getTheMonitor().drawPixelAt(100, 200, "red");
		
		laptop.getTheMotherboard().loadProgram("windows 10");
		
		laptop.getTheCase().pressPower();
		

	}

}
