package com.cerotid.composition;

public class Electronics {
	private int noOfElectronics;
	private String name;
	private Phone phone;
	public Electronics(int noOfElectronics, String name, Phone phone) {
		super();
		this.noOfElectronics = noOfElectronics;
		this.name = name;
		this.phone = phone;
	}
	public int getNoOfElectronics() {
		return noOfElectronics;
	}
	public String getName() {
		return name;
	}
	public Phone getPhone() {
		return phone;
	}
	
	

}
