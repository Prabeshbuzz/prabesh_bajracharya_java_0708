package com.cerotid.composition;

public class Sofa {
	private int noOfSofa;
	private String sofaType;
	private String sofaFabric;
	public Sofa(int noOfSofa, String sofaType, String sofaFabric) {
		super();
		this.noOfSofa = noOfSofa;
		this.sofaType = sofaType;
		this.sofaFabric = sofaFabric;
	}
	public int getNoOfSofa() {
		return noOfSofa;
	}
	public String getSofaType() {
		return sofaType;
	}
	public String getSofaFabric() {
		return sofaFabric;
	}
	
	public void seatingcapacity(int x) {
		System.out.println("This sofa can have "+ x +" number of people in the sofa");
		
	}
	

}
