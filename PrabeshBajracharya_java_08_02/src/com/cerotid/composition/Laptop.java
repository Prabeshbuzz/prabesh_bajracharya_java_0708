package com.cerotid.composition;

public class Laptop {
	private Case therCase;
	private Monitor theMonitor;
	private Motherboard theMotherboard;
	
	public Laptop(Case theCase, Monitor theMonitor, Motherboard theMotherboard) {
		super();
		this.therCase = theCase;
		this.theMonitor = theMonitor;
		this.theMotherboard = theMotherboard;
	}
	public Case getTheCase() {
		return therCase;
	}
	public Monitor getTheMonitor() {
		return theMonitor;
	}
	public Motherboard getTheMotherboard() {
		return theMotherboard;
	}
	
	

}
