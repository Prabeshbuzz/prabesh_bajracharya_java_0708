package com.cerotid.composition;

public class Room {
	private Sofa sofa;
	private Diningtable diningTable;
	private Television television;
	private Studytable studytable;
	private Electronics electronics;

	public Room(Sofa sofa, Diningtable diningTable, Television television, Studytable studytable,
			Electronics electronics) {
		super();
		this.sofa = sofa;
		this.diningTable = diningTable;
		this.television = television;
		this.studytable = studytable;
		this.electronics = electronics;
	}

	public Sofa getSofa() {
		return sofa;
	}

	public Diningtable getDiningTable() {
		return diningTable;
	}

	public Television getTelevision() {
		return television;
	}

	public Studytable getStudytable() {
		return studytable;
	}

	public Electronics getElectronics() {
		return electronics;
	}

}
