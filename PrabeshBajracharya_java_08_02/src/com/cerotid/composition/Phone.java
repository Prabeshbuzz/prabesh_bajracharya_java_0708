package com.cerotid.composition;

public class Phone {
	private String model;

	public void os(String opsys) {
		if (opsys == "ios") {
			System.out.println("its an iphone");
		} else {
			System.out.println("its an android");
		}

	}
}
