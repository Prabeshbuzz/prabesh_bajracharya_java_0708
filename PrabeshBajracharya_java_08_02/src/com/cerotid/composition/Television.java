package com.cerotid.composition;

public class Television {
	private int dimension;
	private String brand;
	public Television(int dimension, String brand) {
		super();
		this.dimension = dimension;
		this.brand = brand;
	}
	public int getDimension() {
		return dimension;
	}
	public String getBrand() {
		return brand;
	}
	
	public void resolution(int a , int b) {
		if( (a + b)>1000 ) {
			System.out.println("display is excellent ");
		}
		else {
			System.out.println("display is poor");
		}
	}

}
