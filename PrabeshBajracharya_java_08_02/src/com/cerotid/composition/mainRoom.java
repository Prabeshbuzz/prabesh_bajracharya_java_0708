package com.cerotid.composition;

public class mainRoom {

	public static void main(String[] args) {
		Phone phone = new Phone();
		Electronics electronics = new Electronics(10, "Sony", phone);
		Studytable studytable = new Studytable();
		Television television = new Television(100, "LG");
		Diningtable diningtable = new Diningtable();	
		Sofa sofa = new Sofa(2, "two seater", "leather");
		Room livingroom = new Room(sofa, diningtable, television, studytable, electronics);
		
		livingroom.getElectronics().getPhone().os("apple");
		livingroom.getTelevision().resolution(1000, 200);
		System.out.println(livingroom.getTelevision().getBrand());
			
			

	}

}
