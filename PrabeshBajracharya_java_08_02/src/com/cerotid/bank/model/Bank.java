package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {
	private final String bankName = "PBC Bank";
	private ArrayList<Customer> customers;
	
	public ArrayList<Customer> getCustomer() {
		return customers;
	}
	public void setCustomer(ArrayList<Customer> customer) {
		this.customers = customer;
	}
	public String getBankName() {
		return bankName;
	}
	
	public void printBankName() {
		System.out.println(getBankName());
	}
	public void printBankDetails() {
		System.out.println(toString());
	}
	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
	
}
