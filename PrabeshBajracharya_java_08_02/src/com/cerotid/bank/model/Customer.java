package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer {
	private String customerName;
	private String address;
	private ArrayList<Account> accounts;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public ArrayList<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}
	
	public void printCustomerAccount() {
		
	}
	public void printCustomerDetails() {
		System.out.println(toString());
		
	}
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", address=" + address + ", accounts=" + accounts + "]";
	}
	
}
