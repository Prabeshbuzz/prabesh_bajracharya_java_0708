package com.cerotid.bank.model;

import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) {
		Bank bank = new Bank();

		Account customer1acct1 = new Account();
		customer1acct1.setAccountType("Checking");

		Account customer1acct2 = new Account();
		customer1acct2.setAccountType("Business Checking");

		Account customer1acct3 = new Account();
		customer1acct3.setAccountType("Saving");

		ArrayList<Account> customer1Accounts = new ArrayList<>();
		customer1Accounts.add(customer1acct1);
		customer1Accounts.add(customer1acct2);
		customer1Accounts.add(customer1acct3);

		Customer customer1 = new Customer();
		customer1.setAddress("2287 Richmond drive,Lithia Springs 30122");
		customer1.setCustomerName("Ramkrishna Dhakal");
		customer1.setAccounts(customer1Accounts);

		Customer customer2 = new Customer();

		Customer customer3 = new Customer();

		ArrayList<Customer> customerList = new ArrayList<>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);

		bank.setCustomer(customerList);

		bank.printBankDetails();
		bank.printBankName();
	}
}
