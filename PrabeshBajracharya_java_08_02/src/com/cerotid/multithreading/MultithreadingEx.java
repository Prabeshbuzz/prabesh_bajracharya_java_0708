package com.cerotid.multithreading;

public class MultithreadingEx extends Thread{
	public void display()
	{
		System.out.println("This is a simple display method ");
	}
	public void run() {
		for( int i = 1;i<10;i++)
		{
			System.out.println("The value of i is "+i);
		}
	}

}
