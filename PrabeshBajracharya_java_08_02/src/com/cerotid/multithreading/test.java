package com.cerotid.multithreading;

public class test {

	public static void main(String[] args) {
		MultithreadingEx thread1 = new MultithreadingEx();
		thread1.start();
		thread1.display();
		
		MultithreadingInterfaceEx thread2 = new MultithreadingInterfaceEx();
		Thread thread3 = new Thread(thread2);
		thread3.start();
		thread2.display();
	}

}
