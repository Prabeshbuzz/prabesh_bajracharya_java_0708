package com.cerotid.Inheritance;

public class Vehicle {
	private int noOfWheels;
	private String engine;
	private String fuel;
	private int topSpeed;
	private String color;
	private String name;

	public Vehicle() {
		this(4, "default", "Petrol", 60, "default", "default");
	}

	public Vehicle(String engine, String fuel, int topSpeed, String color, String name) {
		this(4, engine, fuel, topSpeed, color, name);
	}

	public Vehicle(int noOfWheels, String engine, String fuel, int topSpeed, String color, String name) {
		super();
		this.noOfWheels = noOfWheels;
		this.engine = engine;
		this.fuel = fuel;
		this.topSpeed = topSpeed;
		this.color = color;
		this.name = name;
	}

	public int getNoOfWheels() {
		return noOfWheels;
	}

	public String getEngine() {
		return engine;
	}

	public String getFuel() {
		return fuel;
	}

	public int getTopSpeed() {
		return topSpeed;
	}

	public String getColor() {
		return color;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Vehicle [noOfWheels=" + noOfWheels + ", engine=" + engine + ", fuel=" + fuel + ", topSpeed=" + topSpeed
				+ ", color=" + color + ", name=" + name + "]";
	}

	public void steer() {
		System.out.println("Steer left to turn left and Steer right to turn right");
	}
	
	public void changeGears() {
		System.out.println("Increase speed to change gears ");
	}
	
	public void move()  {
		System.out.println("Press accelerator to get more speed");
	}
}
