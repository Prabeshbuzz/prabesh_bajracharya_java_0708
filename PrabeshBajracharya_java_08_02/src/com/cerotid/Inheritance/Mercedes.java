package com.cerotid.Inheritance;

public class Mercedes extends Car{
	
	private int roadServiceMonths;

	public Mercedes(int noOfWheels, String engine, String fuel, int topSpeed, String color, String name, int doors,
			int gears, boolean isManual, int currentGear) {
		super(noOfWheels, engine, "petrol", 260, color, "Mercedes", 4, gears, true, currentGear);
		this.roadServiceMonths = roadServiceMonths;
	}
	
	public void accelerate(int rate) {
		int newVelocity = getTopSpeed() + rate;
		if(newVelocity == 0) {
			System.out.println("Stop");
		}
		else {
			System.out.println("Change gear "+newVelocity);
		}
	}

	@Override
	public void steer() {
		// TODO Auto-generated method stub
		super.steer();
		System.out.println("Steer aggresively");
	}
	
	

}
