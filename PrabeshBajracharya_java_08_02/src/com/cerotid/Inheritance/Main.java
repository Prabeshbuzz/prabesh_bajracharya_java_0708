package com.cerotid.Inheritance;

public class Main {

	public static void main(String[] args) {
		Vehicle vehicle1 = new Vehicle(4, "v8", "petrol", 160, "Black", "Ford");
		System.out.println(vehicle1);
		
		Mercedes mercedes = new Mercedes(4, null, null, 100, null, null, 0, 0, false, 0);
		mercedes.accelerate(30);
		mercedes.steer();
	}

}
