package com.cerotid.Inheritance;

public class Bus extends Vehicle{
	private int seats;

	public Bus(int seats, int noOfWheels, String engine, String fuel, int topSpeed, String color, String name) {
		super( noOfWheels, engine, fuel, topSpeed, color, name);
		this.seats = seats;
		
	}
	
// is a relationship
}
