package com.cerotid.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HashMapEX {
	
	
	
	public static void main(String[] args) {
		Employee emp1 = new Employee("Prabesh",101,"123-456-789");
		Employee emp2= new Employee("Shraddha",102,"123-456-789");
		Employee emp3 = new Employee("Erica",103,"123-456-789");
		Employee emp4 = new Employee("Rajan",104,"123-456-789");

		Map<Employee,String> values = new HashMap<>();
		values.put(emp1,"User1");
		values.put(emp2,"User2");
		values.put(emp3,"User3");
		values.put(emp4,"User4");
		
		for(Entry<Employee,String> entry : values.entrySet())
		{
			System.out.println(entry.getKey()+" and it is " +entry.getValue());
		}
		

	}
}


