package com.cerotid.collection;

public class Employee {
	private String name;
	private int accountNumber;
	private String ssn;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public Employee(String name, int accountNumber, String ssn) {
		super();
		this.name = name;
		this.accountNumber = accountNumber;
		this.ssn = ssn;
	}
	@Override
	public String toString() {
		return "The details for Employees are as follows : name=" + name + ", accountNumber=" + accountNumber + ", ssn=" + ssn + "]";
	}
	
	

}
