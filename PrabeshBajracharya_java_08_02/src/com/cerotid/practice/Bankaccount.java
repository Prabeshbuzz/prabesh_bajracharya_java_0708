package com.cerotid.practice;

import java.util.Scanner;

public class Bankaccount {
	private int accountNumber;
	private double balance;
	private String customerName;
	private String email;
	private long phoneNumber;
	
	public Bankaccount() {
		this(0000,0.00,"default","default@123.com",000000); // this calls constructor with parameters which is below when nothig provided
		System.out.println("Empty constructor is called");
	}

	public Bankaccount(int accountNumber, double balance, String customerName, String email, long phoneNumber )
	{
		System.out.println("Constructor with parameter called");
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.customerName = customerName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		}
	
	public Bankaccount(String customerName, String email, long phoneNumber) {
		this(11111,0.00,customerName,email,phoneNumber);
		System.out.println("Third type called");
	}
	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void depositFund(double depositAmount) {
		this.balance += depositAmount;
		System.out.println("Deposit of "+ depositAmount + " made, New balance is "+ this.balance);
	}

	public void withdrawFund(double withdrawFund) {
		if(this.balance -withdrawFund <= 0) {
			System.out.println(" Only "+ this.balance + " available withdrawal not possible");
			}
		else {
			this.balance -= withdrawFund;
			System.out.println(" withdrawal of "+ withdrawFund + " processed. Remaining balance "+ this.balance);
		}
	}
}
