package com.cerotid.practice;

public class VipCustomer {
	private String name;
	private double creditLimit;
	private String email;
	
	public VipCustomer() {
		this("default",500.00,"default@example.com");
	}
	public VipCustomer(String name, double creditLimit) {
		this(name,creditLimit,"default@example.com");
	}


	public VipCustomer(String name, double creditLimit, String email) {
		super();
		this.name = name;
		this.creditLimit = creditLimit;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public double getCreditLimit() {
		return creditLimit;
	}
	public String getEmail() {
		return email;
	}
	
	

}
