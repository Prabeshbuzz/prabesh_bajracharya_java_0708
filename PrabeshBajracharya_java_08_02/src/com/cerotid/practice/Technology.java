package com.cerotid.practice;

public class Technology {
	private String java;
	private String dotNet;
	private String sas;
	private String devOps;
	
	public String getJava() {
		return java;
	}
	public void setJava(String java) {
		this.java = java;
	}
	public String getDotNet() {
		return dotNet;
	}
	public void setDotNet(String dotNet) {
		this.dotNet = dotNet;
	}
	public String getSas() {
		return sas;
	}
	public void setSas(String sas) {
		this.sas = sas;
	}
	public String getDevOps() {
		return devOps;
	}
	public void setDevOps(String devOps) {
		this.devOps = devOps;
	}
	
	
}
