package com.cerotid.practice;

public class Main {

	public static void main(String[] args) {
		Technology tech1 = new Technology();
		tech1.setJava("Learning java is fun");
		System.out.println(tech1.getJava());
	//Bank account
		Bankaccount ramAccount = new Bankaccount(123456,1000000,"Krishna","krish@example.com",123456789);
		
		
		ramAccount.depositFund(48000);
		ramAccount.withdrawFund(50000);
		
		Bankaccount hariAccount = new Bankaccount("Hari","hari@example.com", 3456433);
		System.out.println(hariAccount.getAccountNumber());
		
		
		//Vip person
		VipCustomer prabesh = new VipCustomer();
		System.out.println(prabesh.getName());
		
		VipCustomer shraddha = new VipCustomer("Shraddha",50000,"shra@exa.com");
		System.out.println(shraddha.getName());
		
		VipCustomer himal = new VipCustomer("Himal",1222);
		System.out.println(himal.getEmail());
		
	
	}

}
