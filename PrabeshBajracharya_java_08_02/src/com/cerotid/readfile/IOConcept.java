package com.cerotid.readfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class IOConcept {

	public static void main(String[] args) throws IOException {

		Scanner s = new Scanner(new File("customers.txt"));
		ArrayList<String> list = new ArrayList<String>();
		while (s.hasNextLine()) {
			list.add(s.nextLine());
		}
		s.close();
		Collections.sort(list);
		// printing the arraylist using for loop
		for (int i = 0; i < list.size(); i++) {

			System.out.println(list.get(i));

		}
		PrintWriter writer = new PrintWriter("customeroutput.txt");
	    for (String line : list) {
	        writer.println(line);
	    }
	    writer.close();
	

/*
		FileReader reader = new FileReader("customers.txt");
		FileWriter writer = new FileWriter("customeroutput.txt");

		int character;
		while ((character = reader.read()) != -1) {
			writer.write(character);

		}
		
		reader.close();
		writer.close();*/

	}

}
