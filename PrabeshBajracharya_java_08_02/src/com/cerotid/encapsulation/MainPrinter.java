package com.cerotid.encapsulation;

public class MainPrinter {

	public static void main(String[] args) {
		Printer brother = new Printer(-100, 500, false);
		brother.simulatepage(800);
		brother.filltoner(80);

	}

}
