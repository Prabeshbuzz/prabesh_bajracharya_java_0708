package com.cerotid.encapsulation;

public class Printer {
	private int tonerLevel;
	private int numberOfPages;
	private boolean isDuplex;
	
	public int getTonerLevel() {
		return tonerLevel;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public boolean isDuplex() {
		return isDuplex;
	}


	public Printer(int tonerLevel, int numberOfPages, boolean isDuplex) {
		super();
		if(tonerLevel >1 && tonerLevel <=100) {
			this.tonerLevel = tonerLevel;
		} else {
			this.tonerLevel = -1;
		}
		
		this.numberOfPages = 0;
		this.isDuplex = isDuplex;
	}



	public void filltoner(int liquid) {
		if (this.tonerLevel <= 20 && liquid <=80) {
			
			System.out.println("Toner level low " +tonerLevel + " Toner refilling...");
			this.tonerLevel += liquid;
			System.out.println("The new level of toner is "+tonerLevel);
		}
		else {
			System.out.println(" Ink Sufficient to print ");
		}
	}
	public void simulatepage(int noOfPage) {
		this.numberOfPages += noOfPage;
		System.out.println("The total number of page printed is "+this.numberOfPages);
	}

}
