package com.cerotid.employee;

public class testEmployee {

	public static void main(String[] args) {
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		emp1.name = "prabesh Bajracharya";
		emp1.salary = 10000;
		emp1.ssn = "123-45-6789";
		
		emp1.printEmployeeInfo();
		emp1.printSocialSecurityandName();
		
		emp2.name = "Roshan Thapa";
		emp2.salary = 20000;
		emp2.ssn = "122-45-6799";
		System.out.println();
		emp2.printEmployeeInfo();
		emp2.printSocialSecurityandName();
		
		Employee [] empArray = new Employee[2];
		empArray[0] = emp1;
		empArray[1] = emp2;
		
	}
	

}
